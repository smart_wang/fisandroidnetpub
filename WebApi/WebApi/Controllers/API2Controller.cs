﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.Http;

namespace WebApi.Controllers
{
    public class API2Controller : ApiController
    {
        [HttpGet]
        [Route("API2/InsertVisitRecords")]
        public DataSet InsertVisitRecords(String visitName, String visitType, String visitTime, Double lantitude, Double longtitude)
        {
            String connectionString = WebConfigurationManager.AppSettings["AzureDB"];
            DataSet ds = new DataSet();

            try
            {
                if (visitName == null || visitType == null || visitTime == null)
                {
                    DataTable statusTable = GetMessageDataTable(Status.Fail.ToString(), "Something null");
                    ds.Merge(statusTable);
                }
                else
                {
                    String insertCommand = "INSERT INTO SmartVisitRecords " +
                        "(visitName, visitType, visitTime, visitLongtitude, visitLatitude) " +
                        "VALUES(@visitorName, @visitType, @visitTime, @visitLongtitude, @visitLatitude)";

                    SqlConnection connection = new SqlConnection(connectionString);
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(insertCommand, connection))
                    {
                        command.Parameters.Add(new SqlParameter("visitorName", visitName));
                        command.Parameters.Add(new SqlParameter("visitType", visitType));
                        command.Parameters.Add(new SqlParameter("visitTime", visitTime));
                        command.Parameters.Add(new SqlParameter("visitLongtitude", longtitude));
                        command.Parameters.Add(new SqlParameter("visitLatitude", lantitude));
                        command.ExecuteNonQuery();
                    }

                    DataTable messageTable = GetMessageDataTable(Status.Success.ToString(), "");
                    ds.Merge(messageTable);
                }



            }
            catch(Exception ex)
            {
                DataTable messageTable = GetMessageDataTable(Status.Fail.ToString(), ex.Message);
                ds.Merge(messageTable);
            }



            return ds;
        }

        [HttpGet]
        [Route("API2/SelectVisitRecords")]
        public DataSet SelectVisitRecords()
        {
            String connectionString = WebConfigurationManager.AppSettings["AzureDB"];
            DataSet ds = new DataSet();

            try
            {
                SqlConnection connection = new SqlConnection(connectionString);

                String selectCommand = "SELECT * FROM SmartVisitRecords";
                connection.Open();

                using (SqlCommand command = new SqlCommand(selectCommand, connection))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = command;
                    adapter.Fill(ds);
                }

                DataTable statusTable = GetMessageDataTable(Status.Success.ToString(), "");
                ds.Merge(statusTable);

            }
            catch (Exception ex)
            {
                DataTable statusTable = GetMessageDataTable(Status.Fail.ToString(), ex.Message);
                ds.Merge(statusTable);
            }

            return ds;
        }

        private DataTable GetMessageDataTable(String status, String message)
        {
            DataTable table = new DataTable("Message");
            table.Columns.Add("Status");
            table.Columns.Add("Message");
            table.Rows.Add(status, message);

            return table;
        }

    }

    enum Status
    {
        Success,
        Fail
    }
}
