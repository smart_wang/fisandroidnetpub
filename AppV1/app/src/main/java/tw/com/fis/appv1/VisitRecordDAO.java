package tw.com.fis.appv1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by smartwang on 11/04/2017.
 */

public class VisitRecordDAO {
    public static final String TABLE_NAME = "VisitRecord";
    public static final String KEY_ID = "_id";

    public static final String CREATE_TABLE =
            "CREATE TABLE VisitRecord (" +
            "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "visitName TEXT, " +
            "visitType TEXT, " +
            "visitTime TEXT, " +
            "creatDate DATETIME DEFAULT CURRENT_TIMESTAMP)";

    private SQLiteDatabase database;

    public VisitRecordDAO(Context context) {
        database = DBHelper.getDatabase(context);
    }

    public VisitRecord insert(VisitRecord visitRecord) {
        ContentValues cv  = new ContentValues();
        cv.put("visitName", visitRecord.getVisitName());
        cv.put("visitType", visitRecord.getVisitType());
        cv.put("visitTime", visitRecord.getVisitTime());

        long id = database.insert(TABLE_NAME, null, cv);
        visitRecord.setVisitId(id);

        return visitRecord;
    }

    public List<VisitRecord> getAll() {
        List<VisitRecord> recordList = new ArrayList<>();
        // Parameters: TableName, Columns, Where, Where Params, Group, Having, Order, limit
        Cursor cursor = database.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            VisitRecord record = new VisitRecord();
            record.setVisitId(cursor.getLong(0));
            record.setVisitName(cursor.getString(1));
            record.setVisitType(cursor.getString(2));
            record.setVisitTime(cursor.getString(3));

            recordList.add(record);
        }

        return recordList;
    }
}
