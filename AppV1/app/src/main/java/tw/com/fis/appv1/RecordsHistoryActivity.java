package tw.com.fis.appv1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.List;


public class RecordsHistoryActivity extends AppCompatActivity {
    List<VisitRecord> visitRecordList;

    ListView listViewHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_records_history);

        listViewHistory = (ListView)findViewById(R.id.listViewHistory);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Refresh Azure data
//        VisitHistoryTask task = new VisitHistoryTask(this);
//        task.execute();

// SQLite version
        VisitRecordDAO dao = new VisitRecordDAO(RecordsHistoryActivity.this);
        visitRecordList = dao.getAll();

        onDataRefresh(visitRecordList);
    }

    public void onVisit(View view) {
//        Toast.makeText(this, "Hello", Toast.LENGTH_LONG).show();

        Intent intent = new Intent();
        intent.setClass(RecordsHistoryActivity.this, VisitActivity.class);
        startActivity(intent);
    }

    public void onDataRefresh(List<VisitRecord> _records) {
        visitRecordList = _records;

        // Refresh listview data
        VisitHistoryAdapter adapter = new VisitHistoryAdapter(visitRecordList);
        listViewHistory.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
