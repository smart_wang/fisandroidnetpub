package tw.com.fis.appv1;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by smartwang on 07/04/2017.
 */

public class VisitHistoryAdapter extends BaseAdapter {
    List<VisitRecord> recordList;

    public VisitHistoryAdapter(List<VisitRecord> _record) {
        recordList = _record;

        VisitRecord header = new VisitRecord();
        header.setVisitId(0);
        header.setVisitName("姓名");
        header.setVisitType("類型");
        header.setVisitTime("時間");

        recordList.add(0, header);
    }

    @Override
    public int getCount() {
        return recordList.size();
    }

    @Override
    public Object getItem(int position) {
        return recordList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return recordList.get(position).getVisitId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View rootView = inflater.inflate(R.layout.item_visit, parent, false);

        TextView textViewName = (TextView)rootView.findViewById(R.id.textViewName);
        TextView textViewType = (TextView)rootView.findViewById(R.id.textViewType);
        TextView textViewTime = (TextView)rootView.findViewById(R.id.textViewTime);

        VisitRecord visitRecord = recordList.get(position);

        if (position == 0) {
            textViewName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            textViewType.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            textViewTime.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }

        textViewName.setText(visitRecord.getVisitName());
        textViewType.setText(visitRecord.getVisitType());
        textViewTime.setText(visitRecord.getVisitTime());


        return rootView;
    }
}
