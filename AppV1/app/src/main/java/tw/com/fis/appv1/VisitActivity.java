package tw.com.fis.appv1;

import android.location.Location;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Map;

public class VisitActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, OnMapReadyCallback {
    EditText editTextName;
    EditText editTextType;
    EditText editTextTime;

    MapView mapView;
    GoogleMap googleMap;

    GoogleApiClient googleApiClient;

    Double latitude = 0.0;
    Double longtitude = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit);

        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextType = (EditText) findViewById(R.id.editTextType);
        editTextTime = (EditText) findViewById(R.id.editTextTime);

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

    }

    public void onSave(View view) {
        String visitorName = editTextName.getText().toString();
        String visitType = editTextType.getText().toString();
        String visitTime = editTextTime.getText().toString();

        // Web service version
//        VisitInsertTask task = new VisitInsertTask(visitorName, visitType, visitTime, latitude, longtitude, this);
//        task.execute();

        // SQLite version
        VisitRecordDAO dao = new VisitRecordDAO(VisitActivity.this);
        VisitRecord record = new VisitRecord();
        record.setVisitName(visitorName);
        record.setVisitType(visitType);
        record.setVisitTime(visitTime);
        record = dao.insert(record);

        if (record.getVisitId() > 0) {
            Toast.makeText(VisitActivity.this, "Save OK", Toast.LENGTH_LONG).show();
            finish();
        } else {
            Toast.makeText(VisitActivity.this, "Save Fail", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        googleApiClient.connect();
        mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();

        googleApiClient.disconnect();
        mapView.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        mapView.onPause();
    }

    public void onFinish() {
        finish();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {
            Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (location != null) {
                longtitude = location.getLongitude();
                latitude = location.getLatitude();

                LatLng latLng = new LatLng(latitude, longtitude);
                googleMap.addMarker(new MarkerOptions().position(latLng).title("Here"));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));


            }

        } catch (SecurityException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onMapReady(GoogleMap _googleMap) {
        googleMap = _googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

    }
}
