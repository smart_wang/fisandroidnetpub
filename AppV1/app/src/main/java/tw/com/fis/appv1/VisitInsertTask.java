package tw.com.fis.appv1;

import android.os.AsyncTask;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by smartwang on 28/03/2017.
 */
//http://foneshaking.japanwest.cloudapp.azure.com/FISWebApi/API2/SelectVisitRecords
public class VisitInsertTask extends AsyncTask {
    String visitName;
    String visitType;
    String visitTime;
    Double latitude;
    Double longtitude;

    VisitActivity visitActivity;

    public VisitInsertTask(String _visitName, String _visitType, String _visitTime,
                           Double _latitude, Double _longtitude, VisitActivity _activity) {
        this.visitName = _visitName;
        this.visitType = _visitType;
        this.visitTime = _visitTime;
        this.latitude = _latitude;
        this.longtitude = _longtitude;

        this.visitActivity = _activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Object doInBackground(Object[] params) {
        String url = "http://foneshaking.japanwest.cloudapp.azure.com" +
                "/FISWebApi/API2/InsertVisitRecords?" +
                "visitName=" + visitName +
                "&visitType=" + visitType +
                "&visitTime=" + visitTime +
                "&lantitude=" + latitude +
                "&longtitude=" + longtitude;
        String response_string = "";
        String status = "";

        try {
            URL obj = new URL(url);
            HttpURLConnection httpUrlConnection =
                    (HttpURLConnection) obj.openConnection();

            // optional default is GET
            httpUrlConnection.setRequestMethod("GET");

            //add request header
            //httpUrlConnection.setRequestProperty("User-Agent", USER_AGENT);

            response_string = StringUtil.convertInputStream(httpUrlConnection.getInputStream());

            try {

                JSONObject jo = new JSONObject(response_string);


                if( jo.has("Message") ){
                    JSONArray messageArray = jo.getJSONArray( "Message" );
                    if( messageArray.length() > 0 ){
                        JSONObject jo2 = messageArray.getJSONObject(0);
                        if( jo2.has("Status") ){
                            status = jo2.getString( "Status" );
                        }
                        if( jo2.has("Message")){
                            String message = jo2.getString( "Message" );
                        }
                    }
                }

            }catch(Exception e){
                e.printStackTrace();
            }

        } catch (Exception ex) {

        }
        return status;

    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        String status = o.toString();

        if (status.equals("Success")) {
            Toast.makeText(this.visitActivity.getBaseContext(), "Success", Toast.LENGTH_LONG).show();
            this.visitActivity.onFinish();
        } else {
            Toast.makeText(this.visitActivity.getBaseContext(), "Fail", Toast.LENGTH_LONG).show();
        }
    }
}
