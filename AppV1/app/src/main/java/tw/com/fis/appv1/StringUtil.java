package tw.com.fis.appv1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by maxsun on 10/01/2017.
 */
public class StringUtil {

    public static String convertInputStream(InputStream is) {
        try {

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();

            String line = null;

            while ((line = reader.readLine()) != null){
                sb.append(line + "\n");
            }

            return sb.toString();

        } catch (IOException e) {
            //e.printStackTrace();
            return null;
        }

    }
}

