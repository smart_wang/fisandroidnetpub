package tw.com.fis.appv1;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by smartwang on 07/04/2017.
 */

public class VisitHistoryTask extends AsyncTask {
    private List<VisitRecord> recordList = new ArrayList<>();
    private RecordsHistoryActivity activity;

    public VisitHistoryTask(RecordsHistoryActivity _activity) {
        activity = _activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Object doInBackground(Object[] params) {
        String url = "http://foneshaking.japanwest.cloudapp.azure.com" +
                "/FISWebApi/API2/SelectVisitRecords";
        String response_string = "";
        String status = "";

        try {
            URL obj = new URL(url);
            HttpURLConnection httpUrlConnection =
                    (HttpURLConnection) obj.openConnection();

            // optional default is GET
            httpUrlConnection.setRequestMethod("GET");

            //add request header
            //httpUrlConnection.setRequestProperty("User-Agent", USER_AGENT);

            response_string = StringUtil.convertInputStream(httpUrlConnection.getInputStream());

            try {

                JSONObject jo = new JSONObject(response_string);

                if( jo.has("Message") ){
                    JSONArray messageArray = jo.getJSONArray( "Message" );
                    if( messageArray.length() > 0 ){
                        JSONObject jo2 = messageArray.getJSONObject(0);
                        if( jo2.has("Status") ){
                            status = jo2.getString( "Status" );
                        }
                        if( jo2.has("Message")){
                            String message = jo2.getString( "Message" );
                        }
                    }
                }

                if (status.equals("Success")) {
                    if (jo.has("Table")) {
                        JSONArray dataArray = jo.getJSONArray( "Table" );
                        if (dataArray.length() > 0) {
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject data = dataArray.getJSONObject(i);

                                VisitRecord record = new VisitRecord();
                                record.setVisitId(data.getLong("visitId"));
                                record.setVisitName(data.getString("visitName"));
                                record.setVisitType(data.getString("visitType"));
                                record.setVisitTime(data.getString("visitTime"));

                                recordList.add(record);
                            }
                        }
                    }
                }

            }catch(Exception e){
                e.printStackTrace();
            }

        } catch (Exception ex) {

        }
        return status;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        activity.onDataRefresh(recordList);
    }
}
