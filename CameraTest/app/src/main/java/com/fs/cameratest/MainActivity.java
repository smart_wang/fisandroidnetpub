package com.fs.cameratest;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.FileOutputStream;

public class MainActivity extends AppCompatActivity {
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView)findViewById(R.id.imageView);

        SharedPreferences preferences = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("Logined", true);
        editor.commit();
    }

    public void onTakePhoto(View view) {
        SharedPreferences preferences = getPreferences(Context.MODE_PRIVATE);
        Boolean logined = preferences.getBoolean("Logined", false);

        if (logined) {
            Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePhotoIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takePhotoIntent, 1);
            }
        } else {
            Toast.makeText(this, "Not login", Toast.LENGTH_LONG).show();
        }



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap bitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(bitmap);

            String fileName = Environment.getExternalStorageDirectory()
                    .getPath() + "/photo01.jpg";
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(fileName);
                imageView.buildDrawingCache();
                Bitmap myBitmap = imageView.getDrawingCache();
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 50, fileOutputStream);

                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (Exception ex) {
                Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();

            }

        }
    }

    public void onShare(View view) {
        String fileName = Environment.getExternalStorageDirectory()
                .getPath() + "/photo01.jpg";
        imageView.setImageBitmap(BitmapFactory.decodeFile(fileName));

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        Uri uri = Uri.parse("file://" + fileName);
        share.putExtra(Intent.EXTRA_STREAM, uri);

        startActivity(Intent.createChooser(share, "Share Image!"));
    }

    public void onDir(View view) {
        Uri uri = Uri.parse("google.navigation:q=台北101");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.setPackage("com.google.android.apps.maps");
        startActivity(intent);
    }
}
